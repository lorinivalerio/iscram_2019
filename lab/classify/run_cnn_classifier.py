import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
import argparse
from classifier_util import create_text_for_cnn
import io
import json
import csv
import keras.models
from decimal import Decimal
from keras.preprocessing.sequence import pad_sequences
from lab.text_util import die, printe, printable_truncated, tweet_normalization_aggressive, make_normalized_readable, tweet_url
from classifier_util import interpret_label_str2int
import sklearn.externals
import sys


# Main function
def do_main():

    # Command-line arguments
    parser = argparse.ArgumentParser(description='Run a classifier on a set of input files.')
    parser.add_argument('-m', '--model', type=str, required=True, metavar='FILE', dest='model_file_base', help='Classifier, stored in FILE.model.h5 and FILE.tokenizer')
    parser.add_argument('-i', '--input', type=str, required=True, nargs='*', metavar='FILE_JSON', dest='input_files', help='Input file(s) in JSON format')
    parser.add_argument('-o', '--output', type=str, required=False, metavar='FILE_JSON', dest='output_file', help='Output file to write to')
    parser.add_argument('-f', '--output-format', type=str, required=False, default='json', choices=['json', 'csv'], metavar='FORMAT', dest='output_format', help='Output format; use csv to create a spreadsheet for training (in this case, it is strongly suggested to remove duplicates)')
    parser.add_argument('-d', '--remove-duplicates', action='store_true', dest='remove_duplicates', help='Remove duplicates from the output')

    args = parser.parse_args()

    # Deal with output file and output format
    output = None
    output_file = args.output_file
    if output_file:
        printe("Opening output file '%s' for writing" % output_file)
        output = io.open(output_file, 'w')

    keys_to_write = None
    csv_writer = None
    output_format = args.output_format

    if output_file:
        if output_format == 'csv':
            csv_writer = csv.writer(output, delimiter=',')
            keys_to_write = ['tweet_id', 'author', 'url', 'label', 'label_predicted', 'text']
            csv_writer.writerow(keys_to_write)

            if output_file.endswith('.json'):
                printe("*** WARNING: you are writing a .json file but you have specified csv as output format")

        elif output_format == 'json':
            if output_file.endswith('.csv'):
                printe("*** WARNING: you are writing a .csv file but you have specified json as output format")

    # Remove duplicates or not
    remove_duplicates = args.remove_duplicates
    if remove_duplicates:
        printe("Duplicates will be removed")
    else:
        if output_format == 'csv':
            printe("*** WARNING: you are not removing duplicates but generating a csv. It is strongly recommended to use --remove-duplicates")

    # Read tokenizer
    tokenizer_file = args.model_file_base + ".tokenizer"
    printe("Reading tokenizer from '%s'" % tokenizer_file)
    tokenizer = sklearn.externals.joblib.load(tokenizer_file)

    # Read the model
    model_file = args.model_file_base + ".model.h5"
    printe("Reading model from '%s'" % model_file)
    model = keras.models.load_model(model_file)

    # Print the model summary to stderr
    model.summary(print_fn=lambda x: sys.stderr.write(x + '\n'))

    # Determine sequence length
    max_sequence_length = model.layers[0].input_shape[1]
    printe("Max sequence length (second element of layer[0]'s shape): %d" % max_sequence_length)

    # Threshold and counters in case the input file contain some labeled data
    threshold = 0.5
    correctly_classified = 0
    incorrectly_classified = 0

    seen_text = {}

    # Read input files
    for input_file in args.input_files:
        printe("Reading from '%s'" % input_file)
        texts = []
        tweets = []
        with io.open(input_file, "r",encoding='utf-8') as input:
            for line in input:
                tweet = json.loads(line)

                # Deal with both regular and encapsulated tweets
                if "text" in tweet:
                    text = create_text_for_cnn(tweet, [])
                elif "tweet" in tweet and "text" in tweet["tweet"]:
                    text = create_text_for_cnn(tweet["tweet"], [])
                else:
                    die("The tweet must contain a 'text' field or a 'tweet/text' field")

                tweets.append(tweet)
                texts.append(text)

        if len(tweets) == 0:
            printe("-- Empty file, skipping")

        else:
            printe("-- Tokenizing and mapping to sequences")
            tokenizer.oov_token = None
            sequences = tokenizer.texts_to_sequences(texts)

            printe("-- Padding sequences to max. length %d" % max_sequence_length)
            data = pad_sequences(sequences, maxlen=max_sequence_length)

            printe("-- Generating predictions")
            predictions_list = model.predict(data, verbose=1)

            # Column 0 contains the score of the negative class
            # Column 1 contains the score of the positive class
            predictions = zip(tweets, predictions_list[:, 1])

            # Sort by decreasing score and display
            predictions_sorted = sorted(predictions, key=lambda x: x[1], reverse=True)

            for tweet, prob in predictions_sorted:

                if "text" in tweet:
                    text = create_text_for_cnn(tweet, [])
                    id_str = tweet["id_str"]
                    encapsulated = False
                elif "tweet" in tweet and "text" in tweet["tweet"]:
                    text = create_text_for_cnn(tweet["tweet"], [])
                    id_str = tweet["tweet"]["id_str"]
                    encapsulated = True
                else:
                    die("Tweet should contain 'text' or 'tweet/text'")

                if 'label' in tweet:
                    label_int = interpret_label_str2int(tweet['label'])
                    if label_int != 0 and label_int != 1:
                        die("Incorrect label %d for tweet '%s'" % (label_int, text))
                    elif (label_int == 1 and prob >= threshold) or (label_int == 0 and prob < threshold):
                        correctly_classified += 1
                        msg = "OK"
                    else:
                        incorrectly_classified += 1
                        msg = "ERR"
                    print("%.3f\t%s %s\t%s\t%s" % (prob, tweet['label'], msg, id_str, printable_truncated(text, 100)))
                else:
                    print("%.3f\t%s\t%s" % (prob, id_str, printable_truncated(text, 100)))

                # If necessary, also print to output
                if output:
                    if output_format == 'json':
                        if encapsulated:
                            tweet["annotations"] = {"flood_probability": {"yes": str(prob)}}
                        else:
                            tweet["label_predicted"] = str(prob)
                        output.write(json.dumps(tweet))
                        output.write("\n")
                    elif output_format == 'csv':
                        tweet['label_predicted'] = str(prob)
                        if 'id_str' in tweet and 'user' in tweet and 'screen_name' in tweet['user'] and 'text' in tweet:
                            # ['tweet_id', 'author', 'url', 'lael', 'label_predicted', 'text']
                            tweet['tweet_id'] = tweet['id_str']
                            tweet['author'] = tweet['user']['screen_name']
                            tweet['url'] = tweet_url(tweet['id_str'], tweet['user']['screen_name'])
                            tweet['label'] = -1  # empty label
                            tweet['text'] = make_normalized_readable(tweet_normalization_aggressive(tweet['text']))
                            if all(key in tweet for key in keys_to_write):
                                if (not remove_duplicates) or (tweet['text'] not in seen_text):
                                    csv_writer.writerow([tweet[key] for key in keys_to_write])
                                seen_text[tweet['text']] = True
                            else:
                                printe("Missed fields after post-processing tweet, skipping")
                        else:
                            printe("Missed some fields before pre-processing tweet, skipping")
                    else:
                        die("Incorrect output format")

    if correctly_classified + incorrectly_classified > 0:
        printe("\nAccuracy (%d OK/%d Total) = %.2f%%" % (correctly_classified, correctly_classified+incorrectly_classified, 100*correctly_classified / float(correctly_classified+incorrectly_classified)))

    if output:
        printe("Closing output file '%s'" % output_file)
        output.close()


if __name__ == "__main__":
    do_main()
