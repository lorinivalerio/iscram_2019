import argparse
from classifier_util import compute_features_tweet
from classifier_util import read_locations
from classifier_util import compute_metadata_features
from classifier_util import create_text_for_cnn
from classifier_util import test_cnn_tokenizer
import io
import json
import stop_words

def do_main():
    # Command-line arguments
    parser = argparse.ArgumentParser(description='Test tweet representations.')
    parser.add_argument('-i', '--input', type=str, required=True, nargs='*', metavar='FILE_JSON', dest='input_files', help='Labeled input file(s) in JSON format')
    parser.add_argument('-l', '--language', type=str, required=True, metavar='LANG', dest='lang', help='Language for the classifier')
    parser.add_argument('-g', '--locations', type=str, required=True, metavar='FILE_TXT', dest='locations_file', help='File with list of locations found in training data')
    args = parser.parse_args()
        
    # Read locations
    locations = read_locations(args.locations_file)
    print("Locations read: %d" % len(locations))

    # Read annotated tweets
    tweets = []
    for input_file in args.input_files:
        print("Reading from '%s'" % input_file)
        with io.open(input_file, 'r') as input:
            for line in input:
                tweet = json.loads(line)
                if not 'label' in tweet:
                    die("The tweets must contain a 'label' field")
                tweets.append(tweet)
                
    print("Read %d tweets" % len(tweets))
    
    # Get stopwords
    stopwords = dict([(k,True) for k in stop_words.get_stop_words(args.lang)])
    print("Stop words dictionary: %d words" % len(stopwords))
    
    # Process tweets and display them
    for tweet in tweets:
        print("TWEET:")
        print("- Is retweet: %s" % ("YES" if 'retweeted_status' in tweet else "NO"))
        print("- Num retweets of original: %d" % (tweet['retweeted_status']['retweet_count'] if 'retweeted_status' in tweet else 0))
        print("- Is quote: %s" % ("YES" if 'quoted_status' in tweet else "NO"))
        print("- Num quotes of original: %d" % (tweet['quoted_status']['quote_count'] if 'quoted_status' in tweet else 0))
        print("TEXT:")
        print(tweet['text'])
        print()
        
        print("TWITTER user '@%s':" % tweet['user']['screen_name'])
        print("- Followers: %d" % (tweet['user']['followers_count']))
        print("- Verified: %s" % (tweet['user']['verified']))
        print("- URL: %s" % (tweet['user']['url']))
        print("BIO:")
        print(tweet['user']['description'])
        print()
        
        print("METADATA FEATURES:")
        metadata_features_as_dict = compute_metadata_features(tweet)
        print(list(metadata_features_as_dict.keys()))
        print()
        
        print("FEATURES (for standard classifier):")
        features_as_dict = compute_features_tweet(tweet, locations, stopwords)
        print(list(features_as_dict.keys()))
        print()
        
        print("FEATURES (for CNN classifier):")
        text_for_cnn = create_text_for_cnn(tweet, locations)
        print(text_for_cnn)
        print("TOKENIZED:")
        print(test_cnn_tokenizer(text_for_cnn))
        print("================")
                
if __name__ == "__main__":
    do_main()


# python3 classify/test_tweet_representation.py -i ~/inv.sec/2018_jrc_floods/annotated/20171106.training.300.en.json --lang en 
# python3 classify/train_classifier.py --input ~/inv.sec/2018_jrc_floods/annotated/*.training.*.en.json --language en --locations resources/locations.en.txt --test-split 0.2 --algorithm naive-bayes --print-ngrams 30 -o ~/inv.sec/2018_jrc_floods   /models/20180319.relevance.en
# python3 classify/run_classifier.py --language en --input ~/data/2018_floodquerier/*.stage2.en.json --model ~/inv.sec/2018_jrc_floods/models/20180319.relevance.en