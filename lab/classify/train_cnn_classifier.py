
# Based on code by Richard Liao from Dec 26 2016
# Source: https://github.com/richliao/textClassifier/blob/master/textClassifierConv.py

import os
os.environ['KERAS_BACKEND'] = 'tensorflow'

import argparse
import io
import json
from keras.layers import Conv1D
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import MaxPooling1D
from keras.layers import merge
from keras.models import Model
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
import sklearn.externals.joblib
from lab.text_util import die, printe, printable_truncated
from classifier_util import create_text_for_cnn
from classifier_util import create_cnn_tokenizer
from classifier_util import read_locations
from classifier_util import interpret_label_str2int
from copy import copy
import numpy as np
import re

# If we change this parameter, the neural network Flatten() part must
# be modified accordingly.
CNN_MAX_SEQUENCE_LENGTH = 100

# Default embeddings dimension, the embeddings file must use the same dimensionality
DEFAULT_EMBEDDINGS_DIMENSION = 100

# Number of elements over which val_acc is reported
DEFAULT_TEST_PROPORTION = 0.2

# Default number of times the training set is read
DEFAULT_EPOCHS = 10

# Default number of training elements read before adjusting weights and evaluating
DEFAULT_BATCH_SIZE = 128

# Default random seed
DEFAULT_RANDOM_SEED = 7607

# Minimum gain over trivial model to save the model
MINIMUM_ACCURACY_GAIN_OVER_ZERO_R = 0.05

def do_main():
    # Command-line arguments
    parser = argparse.ArgumentParser(description='Creates a Convolutional Neural Network (CNN) classifier from a training set.')
    parser.add_argument('-i', '--input', type=str, required=True, nargs='*', metavar='FILE_JSON', dest='input_files', help='Labeled input file(s) in JSON format')
    parser.add_argument('-s', '--test-split', type=float, required=False, default=DEFAULT_TEST_PROPORTION, metavar='PROB', dest='test_proportion', help='Proportion of input data to use as test set; set to zero if you want to use all the training data for training')
    parser.add_argument('-g', '--locations', type=str, required=True, metavar='FILE_TXT', dest='locations_file', help='File with list of locations found in training data')
    parser.add_argument('-d', '--embeddings-dimension', type=int, required=False, default=DEFAULT_EMBEDDINGS_DIMENSION, metavar='DIM', dest='embeddings_dimension', help='The dimension of the word embedding to be used')
    parser.add_argument('-w', '--embeddings-file', type=str, required=False, metavar='FILE', dest='embeddings_file', help='File containing initialization for word embeddings (one word per line, followed by a space-separated list of numbers of length --embeddings-dimension)')
    parser.add_argument('-n', '--network-size', type=str, required=True, choices=['trivial', 'small', 'medium', 'large'], dest='network_size', help='A network size from one of the pre-defined sizes. Larger networks take longer to train but might be more accurate')
    parser.add_argument('-e', '--epochs', type=int, required=False, default=DEFAULT_EPOCHS, dest='epochs', metavar='NUM', help='Number of epochs, which are passes over the entire training set. More epochs means longer training time but possibly more accuracy')
    parser.add_argument('-b', '--batch-size', type=int, required=False, default=DEFAULT_BATCH_SIZE, dest='batch_size', metavar='NUM', help='Size of a training batch, useful for reporting')
    parser.add_argument('-r', '--random-seed', type=int, required=False, default=DEFAULT_RANDOM_SEED, dest='random_seed', metavar='NUM', help='Random seed used for shuffling training set and for initializing embeddings')
    parser.add_argument('-o', '--output', type=str, required=True, metavar='FILE', dest='output_file', help='Save the classification model to FILE.model.h5 and FILE.tokenizer')
    args = parser.parse_args()

    # Check some arguments
    assert args.embeddings_dimension >= 25, "Embedding dimensions should be >=25"
    assert args.epochs >= 1, "Epochs should be >=1"
    if args.epochs <= 5:
        printe("*** WARNING: the number of epochs you are using is too small %d" % args.epochs)
    assert args.batch_size >= 100, "Batch size should be >= 100"
    assert args.test_proportion >= 0.1 and args.test_proportion <= 0.5, "Test proportion should be in [0.1, 0.5]"

    # Initialize random seed
    if args.random_seed != DEFAULT_RANDOM_SEED:
        print("Initializing random seed to %d" % args.random_seed)
        np.random.seed(args.random_seed)

    # Read locations
    locations = read_locations(args.locations_file)
    print("Locations read: %d" % len(locations))

    # Read tweets
    texts = []
    labels = []

    for input_file in args.input_files:
        with io.open(input_file, 'r') as input:
            print("Reading from '%s'" % input_file)
            for line in input:
                tweet = json.loads(line)
                assert 'text' in tweet, "All tweets must contain a 'label' field"
                text = create_text_for_cnn(tweet, locations)
                assert 'label' in tweet, "All tweets must contain a 'label' field"
                label = interpret_label_str2int(tweet['label'])
                if label != 0 and label != 1:
                    die("Unknown label '%s' for tweet '%s'" % (label, text))
                texts.append(text)
                labels.append(label)

    assert len(texts) == len(labels)
    
    # Compute performance of Zero-Rule classifier
    positive_examples = sum(label == 1 for label in labels)
    negative_examples = sum(label == 0 for label in labels)
    print("Read %d labeled samples (%d positive and %d negative)" % (len(labels), positive_examples, negative_examples))
    
    # Prepare texts by tokenizing
    tokenizer = create_cnn_tokenizer()
    tokenizer.fit_on_texts(texts)
    sequences = tokenizer.texts_to_sequences(texts)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))
    
    sequences_above_max_length = 0
    sequence_sum_len = 0
    for sequence in sequences:
        sequence_sum_len += len(sequence)
        if len(sequence) > CNN_MAX_SEQUENCE_LENGTH:
            sequences_above_max_length += 1
    print("Average sequence length: %.2f" % (sequence_sum_len / len(sequences)))
    
    if sequences_above_max_length > 0:
        print("*** WARNING: %d sequences are above max. length %d and will be truncated" % (sequences_above_max_length, CNN_MAX_SEQUENCE_LENGTH))

    # Pad sequences to fixed CNN_MAX_SEQUENCE_LENGTH
    data = pad_sequences(sequences, maxlen=CNN_MAX_SEQUENCE_LENGTH)
    print('Shape of data tensor (documents x sequence length):', data.shape)

    # Prepare labels
    labels = to_categorical(np.asarray(labels))
    print('Shape of label tensor (documents x categories)', labels.shape)

    indices = np.arange(data.shape[0])
    np.random.shuffle(indices)
    data = data[indices]
    labels = labels[indices]
    
    validation_split = args.test_proportion
    
    nb_validation_samples = int(validation_split * data.shape[0])

    x_train = data[:-nb_validation_samples]
    y_train = labels[:-nb_validation_samples]
    x_val = data[-nb_validation_samples:]
    y_val = labels[-nb_validation_samples:]

    print('Number of positive and negative examples in training (%.1f) and validation (%.1f) set' % (1.0-validation_split, validation_split))
    print('-- Training set:   ' + str(y_train.sum(axis=0)))
    print('-- Validation set: ' + str(y_val.sum(axis=0)))
    
    # Compute validation accuracy of zero-rule classifier
    val_positive_examples = (y_val.sum(axis=0))[0]
    val_negative_examples = (y_val.sum(axis=0))[1]
    if val_positive_examples > val_negative_examples:
        zero_r_accuracy = val_positive_examples / (val_positive_examples + val_negative_examples)
    else:
        zero_r_accuracy = val_negative_examples / (val_positive_examples + val_negative_examples)
    print("Validation accuracy of a classifier that predicts majority class: %.3f" % zero_r_accuracy)


    embeddings_index = {}
    embeddings_dimension = args.embeddings_dimension
    normalization_mapping = {'<user>':'_USER_', '<url>': '_URL_'}
    
    if args.embeddings_file:
        print("Loading word embeddings from '%s' to initialize embedding layer" % args.embeddings_file)
                
        non_word_skipped = 0
        
        with io.open(args.embeddings_file,encoding='utf-8') as embeddings:
            for line in embeddings:
                values = line.split(" ")
                word = values[0]
                if word in normalization_mapping:
                    printe("-- Replacing token '%s' by '%s' to match our normalization" % (word, normalization_mapping[word]))
                    word = normalization_mapping[word]

                if re.match(r'.*[A-Za-z0-9]+.*', word):
                    # Normal word
                    coefs = np.asarray(values[1:], dtype='float32')
                    if len(coefs) != embeddings_dimension:
                        if len(coefs) == 1 and coefs[0] == embeddings_dimension:
                            # do nothing, this is the typical first line of embeddings
                            printe("-- Embeddings file contained header with number of words and number of dimensions (%s %d)" % (values[0], coefs[0]))
                        else:
                            printe("-- Skipping problematic line '%s'" % printable_truncated(line, 75))
                            printe("   because it has wrong number of positions %d, should be %d (is the dimensionality correct?)" % (len(coefs), embeddings_dimension))
                    else:
                        embeddings_index[word] = coefs
                else:
                    # Pure punctuation, or word completely in a non-roman alphabet
                    non_word_skipped += 1
                    
        if non_word_skipped > 0:
            printe("-- Skipped %d tokens that have no roman characters or numbers (do not contain a single character from A-Z, a-z, or 0-9)" % non_word_skipped)
                    
        print('Total %s word vectors of dimension %d loaded.' % (len(embeddings_index), embeddings_dimension))
    else:
        print("*** Warning: word embeddings not initialized, results may be poor")

    #### Fit with a simplified network #####

    # Input Layer
    sequence_input = Input(shape=(CNN_MAX_SEQUENCE_LENGTH,), dtype='int32')

    # Embedding layer
    embedding_matrix = np.random.random((len(word_index) + 1, embeddings_dimension))
    embeddings_found = 0
    for word, i in word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # words not found in embedding index will have the default random vectors
            embedding_matrix[i] = embedding_vector
            embeddings_found += 1
            
    print("Found %d out of %d words (%.1f%%) in the initialized embeddings" % (embeddings_found, len(word_index), 100*embeddings_found/float(len(word_index)) ))

    embedding_layer = Embedding(len(word_index) + 1,
                                embeddings_dimension,
                                weights=[embedding_matrix],
                                input_length=CNN_MAX_SEQUENCE_LENGTH,
                                trainable=True)

    embedded_sequences = embedding_layer(sequence_input)

    if args.network_size == 'trivial':  
        # Not sure if this one works at all!
        l_flat = Flatten()(embedded_sequences)
        l_dense = Dense(128, activation='relu')(l_flat)
        preds = Dense(2, activation='softmax')(l_dense)
    
    elif args.network_size == 'small':
        
        # [Note 1] If you get this error:
        # 'The shape of the input to "Flatten" is not fully defined (got (0, 128)...'
        # you need to reduce the coefficient inside the last MaxPooling1D
        # .... but not too much!
        # make sure the Flatten layer has 128 output neurons.
        
        l_cov1 = Conv1D(128, 5, activation='relu')(embedded_sequences)
        l_pool1 = MaxPooling1D(5)(l_cov1)
        l_cov2 = Conv1D(128, 5, activation='relu')(l_pool1)
        l_pool2 = MaxPooling1D(5)(l_cov2)   # Use 20 if CNN_MAX_SEQUENCE_LENGTH=200
        l_flat = Flatten()(l_pool2)         # See [Note 1] above.
        l_dense = Dense(128, activation='relu')(l_flat)
        preds = Dense(2, activation='softmax')(l_dense)

    elif args.network_size == 'medium':
        l_cov1 = Conv1D(128, 5, activation='relu')(embedded_sequences)
        l_pool1 = MaxPooling1D(3)(l_cov1)
        l_cov2 = Conv1D(128, 5, activation='relu')(l_pool1)
        l_pool2 = MaxPooling1D(3)(l_cov2)
        l_cov3 = Conv1D(128, 5, activation='relu')(l_pool2)
        l_pool3 = MaxPooling1D(3)(l_cov3)    
        l_flat = Flatten()(l_pool3)         # See [Note 1] above.
        l_dense = Dense(128, activation='relu')(l_flat)
        preds = Dense(2, activation='softmax')(l_dense)

    elif args.network_size == 'large':

        # applying a more complex convolutional approach
        convs = []
        filter_sizes = [3, 4, 5]

        for fsz in filter_sizes:
            l_conv = Conv1D(filters=128, kernel_size=fsz, activation='relu')(embedded_sequences)
            l_pool = MaxPooling1D(5)(l_conv)
            convs.append(l_pool)

        l_merge = merge(convs, mode='concat', concat_axis=1)
        l_cov1 = Conv1D(128, 5, activation='relu')(l_merge)
        l_pool1 = MaxPooling1D(5)(l_cov1)
        l_cov2 = Conv1D(128, 5, activation='relu')(l_pool1)
        l_pool2 = MaxPooling1D(5)(l_cov2)
        l_flat = Flatten()(l_pool2)         # See [Note 1] above.
        l_dense = Dense(128, activation='relu')(l_flat)
        preds = Dense(2, activation='softmax')(l_dense)

    else:
        die("Unrecognized network size '%s'" % args.network_size)

    # Compile and display model
    model = Model(sequence_input, preds)
    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['acc'])
    model.summary()
    
    # Perform fitting
    history = model.fit(x_train, y_train, validation_data=(x_val, y_val),
              epochs=args.epochs, batch_size=args.batch_size)
          
    # Check the classifier actually achieved something
    last_val_acc = (history.history['val_acc'])[-1]
    print("Last validation accuracy: %.3f; trivial validation accuracy %.3f" % (last_val_acc, zero_r_accuracy))
    if last_val_acc < zero_r_accuracy + MINIMUM_ACCURACY_GAIN_OVER_ZERO_R:
        
        # Nothing was gained, abort
        print("\n*** ATTENTION.")
        print("The accuracy of the model is too low compared with the trivial classifier,")
        print("which means the classifier is bad or just outputs the majority class. Possible solutions:")
        print("(1) Review your training data (2) Discard some training elements")
        print("(3) Change type of network (4) Change test split size")
        print("(5) Repeat with more epochs (6) Repeat with a different random seed")
        print("ABORTING. No file generated")
    
    else:
        
        # Save model
        if args.output_file:
            model_file = args.output_file + ".model.h5"
            tokenizer_file = args.output_file + ".tokenizer"
            metadata_filename = args.output_file + ".metadata"
            
            model.save(model_file)
            sklearn.externals.joblib.dump(tokenizer, tokenizer_file)
            
            # This should be done with an ordered dictionary instead
            metadata = {}
            metadata['rows'] = {}
            metadata['rows']['training'] = len(y_train)
            metadata['rows']['validation'] = len(y_val)
            metadata['labels'] = {}
            metadata['labels']['positive'] = positive_examples
            metadata['labels']['negative'] = negative_examples
            metadata['max_sequence_length'] = CNN_MAX_SEQUENCE_LENGTH
            metadata['embeddings'] = {}
            metadata['embeddings']['dimensionality'] = embeddings_dimension
            metadata['embeddings']['words_available'] = len(embeddings_index)
            metadata['embeddings']['words_seen'] = len(word_index)
            metadata['embeddings']['words_seen_and_available'] = embeddings_found
            metadata['validation_accuracy_final'] = history.history['val_acc'][-1]
            metadata['validation_accuracy_by_epoch'] = history.history['val_acc']
            metadata['args'] = copy(vars(args))
            with io.open(metadata_filename, 'w') as metadata_file:
                json.dump(metadata, metadata_file, indent=4)
            
            print("Saved model to:\n- %s\n- %s\n- %s" % (model_file, tokenizer_file, metadata_filename))

if __name__ == "__main__":
    do_main()
