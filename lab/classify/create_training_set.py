import argparse
import csv
import io
import json
from lab.text_util import tweet_normalization_aggressive, make_normalized_readable, tweet_url, printe, CustomJSONEncoder
import numpy.random
import time

def get_date(tweet):
    assert 'created_at' in tweet, "Tweet does not contain a 'created_at' field"
    tweet_created_at = time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')
    date = time.strftime("%Y-%m-%d", tweet_created_at)
    return date


def do_main():
    # Command-line arguments
    smfr_json_encoder = CustomJSONEncoder().default

    parser = argparse.ArgumentParser(description='Create a training set from an input set of files, removing duplicates.')
    parser.add_argument('-s', '--sample-size', type=int, required=True, metavar='COUNT', dest='sample_size', help='Size of sample to generate')
    parser.add_argument('-i', '--input', type=str, required=True, nargs='*', metavar='FILE_JSON', dest='input_files', help='Input file(s) in JSON format')
    parser.add_argument('-f', '--force-include', type=str, required=False, nargs='*', default=[], metavar='FILE_CSV', dest='force_include_files', help='Force the inclusion of tweets with these tweet-ids, does not count against the sample size')
    parser.add_argument('-x', '--exclude', type=str, required=False, nargs='*', default=[], metavar='FILE_JSON', dest='exclude_files', help='Exclude tweets already in these training files(s) in JSON format')
    parser.add_argument('-o', '--output', type=str, required=True, metavar='FILE_BASENAME', dest='output_file_basename', help='Basename of file to write the training set to')
    parser.add_argument('-m', '--max-per-day', type=int, required=False, default=-1, metavar='COUNT', dest='max_per_day', help='Max. number of samples belonging to a single day to generate')
    args = parser.parse_args()

    # Read forced inclusion file
    force_include = {}
    for exclude_file in args.force_include_files:
        printe("Reading forced inclusions from '%s'" % exclude_file)
        with io.open(exclude_file, 'r',encoding="utf-8", errors="ignore") as input:
            for line in input:
                tweet_id = line.strip()
                force_include[tweet_id] = True
    print("Read %d included tweet-ids which will be forced to be included" % len(force_include))

    # Read input files
    seen_text = {}
    lines = 1
    read = []
    tweetid_to_date = {}
    tweets_per_date = {}

    seen_forced_inclusion = 0
    for input_file in args.input_files:
        printe("Reading from '%s'" % input_file)
        with io.open(input_file, 'r',encoding="utf-8", errors="ignore") as input:
            for line in input:
                try:
                    status = json.loads(line)
                    tweet_id = status['id_str']
                    author = status['user']['screen_name']
                    if 'text' in status:
                        text = tweet_normalization_aggressive(status['text'])
                        if tweet_id in force_include or not text.lower() in seen_text:
                            if tweet_id in force_include:
                                seen_forced_inclusion += 1
                            read.append({
                                'tweet_id': tweet_id,
                                'author': author,
                                'url': tweet_url(tweet_id, author),
                                'text': make_normalized_readable(text),
                                'tweet': status,
                                'label': (status['label'] if 'label' in status else ''), # Preserve possible label in the tweet
                            })
                            date = get_date(status)
                            tweetid_to_date[tweet_id] = date
                            tweets_per_date[date] = tweets_per_date[date] + 1 if date in tweets_per_date else 1
                        seen_text[text.lower()] = True

                except Exception as e:
                    printe("Skipping tweet in line %d: %s" % (lines, e))

                lines += 1
                if lines % 10000 == 0:
                    printe("- Tweets read: %d" % lines )



    printe("Read %d unique tweets in total" % len(read))
    printe("Found %d/%d forced inclusion tweets with unique text" % (seen_forced_inclusion, len(force_include)))

    # Read exclusion file
    exclude = {}
    for exclude_file in args.exclude_files:
        printe("Reading exclusions from '%s'" % exclude_file)
        with io.open(exclude_file, 'r',encoding="utf-8", errors="ignore") as input:
            for line in input:
                status = json.loads(line)
                tweet_id = status['id_str']
                exclude[tweet_id] = True
    print("Read %d excluded tweets which are already on a training set" % len(exclude))

    written_per_date = dict([(date,0) for date in tweets_per_date.keys()])
    keys_to_write = ['tweet_id', 'author', 'url', 'label', 'text']

    # Sample selected
    sample = numpy.random.permutation(read)
    selected = []
    included_forced = 0
    sampled = 0
    for element in sample:
        if element['tweet_id'] in force_include:
            selected.append(element)
            included_forced += 1

        elif not element['tweet_id'] in exclude and sampled <= args.sample_size:
            date = tweetid_to_date[element['tweet_id']]
            if args.max_per_day == -1 or written_per_date[date] < args.max_per_day:
                selected.append(element)
                written_per_date[date] += 1
                sampled += 1

    written = 0
    output_file_json = "%s.json" % args.output_file_basename
    output_file_csv = "%s.csv" % args.output_file_basename

    with io.open(output_file_json, 'w', encoding='utf-8') as output_json, io.open(output_file_csv, 'w', encoding='utf-8') as output_csv:
        print("Writing to '%s' and '%s'" % (output_file_json, output_file_csv))
        writer = csv.writer(output_csv, delimiter=',')
        writer.writerow(keys_to_write)
        for element in selected:
            written += 1
            # Write to CSV file for annotation
            writer.writerow([element[key] for key in keys_to_write])
            # Write to JSON file
            output_json.write(json.dumps(element['tweet'], ensure_ascii=False,default=smfr_json_encoder))
            output_json.write("\n")

    dates_sorted = sorted(tweets_per_date.keys())
    print("Sampled tweets (%d in total) per date:" % sampled)
    for date in dates_sorted:
        print("-- %s: %d (%d written)" % (date, tweets_per_date[date], written_per_date[date]))

    print("Plus %d tweets that were forced to be included" % (included_forced) )
    print("Wrote total %d tweet" % written)


if __name__ == "__main__":
   do_main()
