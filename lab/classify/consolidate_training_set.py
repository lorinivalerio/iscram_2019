import argparse
import csv
import io
import json
import re
from lab.text_util import die, printe, tweetid_from_url

def normalize_label(label):
    if label == '0' or label == '1' or label == '2':
        return label
    elif re.match('no', label, re.IGNORECASE):
        return 0
    elif re.match('yes *\(other\)', label, re.IGNORECASE):
        return 1
    elif re.match('yes *\(eyewitness\)', label, re.IGNORECASE):
        return 2
    elif label == '-1':
        return -1
    else:
        return -2

def do_main():
    # Command-line arguments
    parser = argparse.ArgumentParser(description='Parses a training set.')
    parser.add_argument('-i', '--input', type=str, required=True, nargs='*', metavar='FILE_JSON', dest='json_files', help='Input file(s) in JSON format')
    parser.add_argument('-a', '--annotated', type=str, required=True, nargs='*', metavar='FILE_CSV', dest='annotated_files', help='Annotated file(s) in CSV format')
    parser.add_argument('-o', '--output', type=str, required=True, metavar='FILE_JSON', dest='output_file', help='File to write the annotated tweets in JSON')
    args = parser.parse_args()

    # Read the file with annotations
    annotations = {}
    for annotated_file in args.annotated_files:
        printe("Reading annotations from '%s'" % annotated_file)
        # This is one of the few exceptional cases in which it is OK to ignore
        # an UTF-8 error, we do it because we are *not* using the text from the
        # csv file (we obtain the text from the JSON file)
        with io.open(annotated_file, encoding="utf-8", errors="ignore") as input:
            reader = csv.reader(input, delimiter=",")

            # Read headers
            headers = next(reader)[0:]
            if 'url' not in headers:
                die("The headers of the annotation file do not include a 'url' field (needed to determine tweet_id)")
            if 'label' not in headers:
                die("The headers of the annotation file do not include a 'label' field")

            # Read lines
            duplicate_annotations = 0
            for line in reader:
                tweet = {key: value for key, value in zip(headers, line[0:])}
                tweet_id = tweetid_from_url(tweet['url'])
                if tweet_id in annotations:
                    duplicate_annotations += 1
                annotations[tweet_id] = tweet

    printe("Number of annotations read: %d" % len(annotations))
    if duplicate_annotations > 0:
        printe("WARNING: there are %d tweet-ids annotated more than once, kept the last label only" % duplicate_annotations)

    # Read the original files
    annotations_written = 0
    with io.open(args.output_file, 'w') as output:
        printe("Writing to '%s'" % args.output_file)
        for input_file in args.json_files:
            printe("Reading from '%s'" % input_file)
            with io.open(input_file, 'r') as input:
                for line in input:
                    status = json.loads(line)

                    # Deal with encapsulated tweets
                    if "text" in status:
                        tweet_id = status["id_str"]
                    elif "tweet" in status and "text" in status["tweet"]:
                        tweet_id = status["tweet"]["id_str"]
                    else:
                        die("Unknown tweet format in input")

                    # Find the annotation
                    if tweet_id in annotations:
                        status['label'] = annotations[tweet_id]['label']
                        label = normalize_label(status['label'])
                        if label == -1:
                            printe("Skipping tweet %s labeled -1 (meaning no annotation)" % tweet_id)
                        elif label == -2:
                            die("Unrecognized label '%s' on tweet '%s' (must be 0, 1, or 2)" % (status['label'], status['id_str']))
                        else:
                            status['label'] = label
                            output.write(json.dumps(status))
                            output.write("\n")
                            annotations_written += 1

    printe("Number of annotations written: %d" % annotations_written)


if __name__ == "__main__":
    do_main()
