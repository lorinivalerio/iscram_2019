import argparse
from classifier_util import compute_features_tweet
import io
import json
from lab.text_util import printe
import sklearn.externals
import stop_words


# Main loop
def do_main():

    # Command-line arguments
    parser = argparse.ArgumentParser(description='Run a classifier on a set of input files.')
    parser.add_argument('-m', '--model', type=str, required=True, metavar='FILE', dest='model_file_base', help='Classifier, stored in FILE.model and FILE.vectorizer')
    parser.add_argument('-i', '--input', type=str, required=True, nargs='*', metavar='FILE_JSON', dest='input_files', help='Input file(s) in JSON format')
    parser.add_argument('-l', '--language', type=str, required=True, metavar='LANG', dest='lang', help='Language for the classifier')
    args = parser.parse_args()

    # Read the model
    model_file = args.model_file_base + ".model"
    print("Reading model from '%s'" % model_file)
    model = sklearn.externals.joblib.load(model_file)
    vectorizer_file = args.model_file_base + ".vectorizer"
    print("Reading vectorizer from '%s'" % vectorizer_file)
    vectorizer = sklearn.externals.joblib.load(vectorizer_file)

    # Get stopwords
    stopwords = dict([(k, True) for k in stop_words.get_stop_words(args.lang)])
    print("Stop words dictionary: %d words" % len(stopwords))

    # Read input files
    for input_file in args.input_files:
        printe("Reading from '%s'" % input_file)
        predictions = []
        with io.open(input_file, 'r') as input:
            for line in input:
                tweet = json.loads(line)
                features_as_dict = compute_features_tweet(tweet, [], stopwords)
                features_as_vector = vectorizer.transform([features_as_dict])
                predict_class1 = model.predict_proba(features_as_vector)[0][1]
                predictions.append((tweet, predict_class1))

        predictions_sorted = sorted(predictions, key=lambda x: x[1], reverse=True)
        for tweet, prob in predictions_sorted:
            printable_text = tweet['text'].replace("\n", "")[:100]
            print("%.3f\t%s\t%s" % (prob, tweet['id_str'], printable_text))


if __name__ == "__main__":
    do_main()
