import argparse
import configparser
import csv
import io
from lab.text_util import die, printe, tweetid_from_url

def get_questions_answers(config):
    questions_answers = {}
    section_num = 1
    while "Labels %d" % section_num in config:
        section = config["Labels %d" % section_num]
        question = section['question']
        answer_num = 1
        answer_map = {}
        while "answer_%d" % answer_num in section:
            answer = section["answer_%d" % answer_num]
            code = section["code_%d" % answer_num]
            answer_map[answer] = int(code)
            answer_num += 1
        questions_answers[question] = answer_map
        section_num += 1
    
    return(questions_answers)

def average_annotations(tweets, question, answers):
    count = 0
    total = 0
    user_id = []
    for tweet in tweets:
        answer = answers[tweet[question]]
        count += 1
        total += answer
        user_id.append(tweet['User ID'])
    
    result = tweets[0]
    result['User ID'] = ",".join(user_id)
    result['label'] = float(total) / float(count)
    del(result[question])
    return(result)
    
def do_main():
    # Command-line arguments
    parser = argparse.ArgumentParser(description='Parses annotation results from crowdsourcing site gethybrid.io')
    parser.add_argument('-c', '--config', type=str, required=True, metavar='FILE_INI', dest='config_file')
    parser.add_argument('-a', '--annotated-file', type=str, required=True, metavar='FILE_CSV', dest='annotated_file', help='Annotated file in CSV format, downloaded from gethybrid.io')
    parser.add_argument('-o', '--output-file', type=str, required=True, metavar='FILE_CSV', dest='output_file', help='Consolidated annotated file')
    args = parser.parse_args()

    # Read questions and answers ("qa") from config file
    config = configparser.RawConfigParser()
    config.read(args.config_file)
    questions_answers = get_questions_answers(config)

    # Read the file with annotations
    printe("Reading annotations from '%s'" % args.annotated_file)
    annotations = {}
    question = ''
    with io.open(args.annotated_file, 'r') as input:
        reader = csv.reader( input, delimiter="," )
        
        # Read headers
        column_names = next(reader)[0:]
        
        # Check mandatory columns
        for required_column in ['tweet_id', 'url', 'text', 'User ID', 'author']:
            if not required_column in column_names:
                die("The headers of the annotation file do not include a '%s' field" % required_column)
                
        # Check that there is one and only one question
        for column_name in column_names:
            if column_name in questions_answers:
                if len(question) == 0:
                    question = column_name
                else:
                    die("More than one question defined")
        
        if len(question) == 0:
            die("No known question (from the configuration file) is a column in the input file")
                
        # Read lines
        for line in reader:
            tweet = {key: value for key, value in zip(column_names, line[0:])}
            tweet_id = tweetid_from_url(tweet['url'])
            
            if 'label' in tweet and len(tweet['label']) > 0:
                die("A tweet includes a non-empty label (tweet_id=%s)" % tweet_id)
            
            if tweet_id in annotations:
                annotations[tweet_id].append(tweet)
            else:
                annotations[tweet_id] = [tweet]

    printe("Number of annotated tweets read: %d" % len(annotations))
    
    # Average the labels
    averaged = []
    for tweet_id in annotations:
        tweet = average_annotations(annotations[tweet_id], question, questions_answers[question])
        averaged.append(tweet)
    averaged = sorted(averaged, key = lambda x: x['label'])    
    printe("Averaged labels of %d tweets" % len(averaged))
   
    # Write to putput
    output_header = ['tweet_id', 'author', 'url', 'label', 'text', 'User ID']
    with io.open(args.output_file, 'w') as output:
        writer = csv.writer(output, delimiter=',')
        writer.writerow(output_header)
        for tweet in averaged:
            writer.writerow([tweet[key] for key in output_header])
    
    printe("Wrote to '%s'" % args.output_file)
    printe("You may want to edit that file to convert average labels to a single value")

if __name__ == "__main__":
    do_main()
