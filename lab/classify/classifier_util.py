
import geotext
import io
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.text import text_to_word_sequence
from lab.text_util import get_ngrams
from lab.text_util import tweet_normalization_aggressive
from lab.text_util import die
import math
import numpy as np
import operator
import re

# You may increase this if the "Found NNNN unique tokens" message in 
# train_cnn_classifier gets too close to this number. As a reference,
# with 4K+ training examples we get about 11K different words.
MAX_NB_WORDS = 40000

# This is Keras' default except the '_' and '#' characters, which we keep
CNN_TOKENIZER_FILTERS = '!"$%&()*+,-./:;<=>?@[\\]^`{|}~\t\n'

def create_cnn_tokenizer():
    """Creates a tokenizer. Make sure test_cnn_tokenizer uses the same configuration
    except for the num_words parameter."""
    return Tokenizer(num_words=MAX_NB_WORDS,
        filters=CNN_TOKENIZER_FILTERS,
        lower=True,
        split=" ")

def test_cnn_tokenizer(text):
    """Test the tokenizer, must use exactly the same configuration as create_cnn_tokenizer"""
    return text_to_word_sequence(text,
        filters=CNN_TOKENIZER_FILTERS,
        lower=True,
        split=" ")
        
def replace_locations_loc_text(raw_text, locations):
    """Replace locations in text by "_loc_" (before normalization)"""
    places = geotext.GeoText(raw_text)
    places_list = places.cities + places.countries
    lowercase_text_without_locations = raw_text.lower()
    for location in places_list + locations:
        lowercase_text_without_locations = re.sub(r"\b" + location.lower() + r"\b", '_loc_', lowercase_text_without_locations)

    return lowercase_text_without_locations

def create_text_for_cnn(tweet, locations):
    """Tokenization/string cleaning for conv neural network: removes all quote characters;
    it also append metadata features at the end of the tweet."""
    
    # Remove locations
    lowercase_text_without_locations = replace_locations_loc_text(tweet['text'], locations)
    
    string = tweet_normalization_aggressive(lowercase_text_without_locations)
    
    string = re.sub(r"\\", "", string)    
    string = re.sub(r"\'", "", string)    
    string = re.sub(r"\"", "", string)
    
    # Add metadata features
    metadata_as_dict = compute_metadata_features(tweet)

    string += " " + " ".join(sorted(list(metadata_as_dict.keys())))
    return string.strip().lower()

def compute_features_text(raw_text, locations, stopwords):
    '''Compute features from text'''
    
    lowercase_text_without_locations = replace_locations_loc_text(raw_text, locations)
        
    # Normalize text
    normalized_text = tweet_normalization_aggressive(lowercase_text_without_locations)
    ngrams = get_ngrams(normalized_text)
    ngrams_as_features = dict([ (ngram,1) for ngram in ngrams])
    
    # Remove features made of a single stopword
    for stopword in stopwords:
        if stopword in ngrams_as_features:
            del(ngrams_as_features[stopword])
                
    # Remove features made of a single character that is not a hashtag
    for feature in list(ngrams_as_features.keys()):
        if feature != '#' and len(feature) == 1:
            del(ngrams_as_features[feature])

    return(ngrams_as_features)

def compute_pow10_feature(number):
    """Given a positive number, expresses it as a power of 10, e.g.: 3421 -> 1000s"""
    if number > 0:
        return("%d" % math.pow(10, math.floor(math.log10(number))))
    else:
        return("0")

def compute_metadata_features(tweet):
    """Compute features from a tweet's metadata"""
    features = []
    
    # User
    followers_count = tweet['user']['followers_count']
    features.append("_user_followers_%ss" % compute_pow10_feature(followers_count))

    verified = tweet['user']['verified']
    if verified:
        features.append("_user_is_verified")
        
    url = tweet['user']['url']
    if url:
        features.append("_user_has_url")
        
    bio = tweet['user']['description']
    if bio:
        features.append("_user_has_bio")
    
    # Tweet is RT or quote
    # (note that the retweet_count of a tweet obtained via filter is always zero
    #  because we have collected it the moment it was posted, so we focus
    #  on the number of RTs/quotes of its original tweet)
    retweets_or_quotes = 0
    if 'retweeted_status' in tweet:
        retweets_or_quotes += tweet['retweeted_status']['retweet_count']
        
    if 'quoted_status' in tweet and 'quote_count' in tweet['quoted_status']:
        retweets_or_quotes += tweet['quoted_status']['quote_count']
        
    if retweets_or_quotes > 0:
        features.append("_retweet_or_quote_count_%ss" % compute_pow10_feature(retweets_or_quotes))
    
    # Create as a dictionary and return
    metadata_as_features = dict([ (feature,1) for feature in features])
    return(metadata_as_features)

def merge_two_dicts(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return(z)

def compute_features_tweet(tweet, locations, stopwords):
    return merge_two_dicts(
        compute_features_text(tweet['text'], locations, stopwords),
        compute_metadata_features(tweet)
    )

def read_locations(locations_file):
    locations = []
    with io.open(locations_file, 'r',encoding='utf-8') as input:
        for line in input:
            if not line.startswith('#'):
              locations.append(line.rstrip())
    return locations

def get_tree_properties(classifier):
    """ Source: http://scikit-learn.org/stable/auto_examples/tree/plot_unveil_tree_structure.html """
    n_nodes = classifier.tree_.node_count
    children_left = classifier.tree_.children_left
    children_right = classifier.tree_.children_right
    feature = classifier.tree_.feature
    threshold = classifier.tree_.threshold
    value = classifier.tree_.value

    # The tree structure can be traversed to compute various properties such
    # as the depth of each node and whether or not it is a leaf.
    node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    is_leaves = np.zeros(shape=n_nodes, dtype=bool)
    stack = [(0, -1)]  # seed is the root node id and its parent depth
    while len(stack) > 0:
        node_id, parent_depth = stack.pop()
        node_depth[node_id] = parent_depth + 1

        # If we have a test node
        if (children_left[node_id] != children_right[node_id]):
            stack.append((children_left[node_id], parent_depth + 1))
            stack.append((children_right[node_id], parent_depth + 1))
        else:
            is_leaves[node_id] = True

    return(n_nodes, children_left, children_right, feature, threshold, value, node_depth, is_leaves)

def dump_tree(classifier, vectorizer):

    n_nodes, children_left, children_right, feature, threshold, value, node_depth, is_leaves = get_tree_properties(classifier)
    feature_names = vectorizer.feature_names_

    print("The binary tree structure has %s nodes and has "
          "the following tree structure:"
          % n_nodes)
    for i in range(n_nodes):
        if is_leaves[i]:
            str_val = value[i][0][1] / (value[i][0][0] + value[i][0][1])
            #print("%snode=%s leaf node %s" % (node_depth[i] * " ", i, value[i]))
            print("%snode=%s leaf node %.2f" % (node_depth[i] * " ", i, str_val))
        else:
            print("%snode=%s test node: go to node %s if '%s' <= %s else to "
                  "node %s."
                  % (node_depth[i] * " ",
                  i,
                  children_left[i],
                  feature_names[feature[i]],
                  threshold[i],
                  children_right[i],
                  ))
    print()

def dump_tree_ngrams(classifier, vectorizer, count):
    n_nodes, children_left, children_right, feature, threshold, value, node_depth, is_leaves = get_tree_properties(classifier)
    feature_names = vectorizer.feature_names_
    
    internal_nodes = []
    for i in range(n_nodes):
        if not is_leaves[i]:
            node = {}
            node['depth'] = node_depth[i]
            node['ngram'] = feature_names[feature[i]]
            internal_nodes.append(node)
    
    ordered_nodes_by_depth = sorted(internal_nodes, key=lambda x: x['depth'])
    
    print("Ngrams by minimum depth at which they appear (0=root)")
    seen = {}
    to_print = count
    for node in ordered_nodes_by_depth:
        depth, ngram = (node['depth'], node['ngram'])
        if not ngram in seen:
            print("- %d\t%s" % (depth, ngram))
            seen[ngram] = True
            to_print = to_print -1
            if to_print == 0:
                break

def dump_multinomial_nb_ngrams(classifier, vectorizer, count):
    feature_names = vectorizer.feature_names_
    
    for i in range(0, len(classifier.feature_log_prob_)):
        print("\nMost informative features for class %d" % i)
        probabilities = [math.exp(x) for x in classifier.feature_log_prob_[i]]
        prob_dict = dict(zip(feature_names, probabilities))
        
        ordered_prob_dict = sorted(prob_dict.items(), key=operator.itemgetter(1), reverse=True)
        to_print = count
        for ngram, prob in ordered_prob_dict:
            print("- %s\t%f" % (ngram, prob))
            to_print = to_print -1
            if to_print == 0:
                break
 
def dump_svm_ngrams(classifier, vectorizer, count):
    feature_names = vectorizer.feature_names_
    
    print("\nMost informative features")
    prob_dict = dict(zip(feature_names, classifier.coef_[0].toarray()[0]))

    ordered_prob_dict = sorted(prob_dict.items(), key=operator.itemgetter(1), reverse=True)
    to_print = count
    for ngram, coeff in ordered_prob_dict:
        print("- %s\t%f" % (ngram, coeff))
        to_print = to_print -1
        if to_print == 0:
            break
            
            
def interpret_label_str2int(label):
    if label == '0' or label == 0:
        return 0
    elif label == '1' or label == '2' or label == 1 or label == 2:
        return 1
    else:
        die("Unknown label '%s' in training set" % label)

