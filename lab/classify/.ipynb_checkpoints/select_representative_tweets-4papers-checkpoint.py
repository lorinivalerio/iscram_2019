
import argparse
import json
import io
from lab.text_util import printe, die, printable_truncated, tweet_normalization_aggressive, tweet_url
from Levenshtein import ratio

# Number of tweets to return
DEFAULT_COUNT_TWEETS = 5

# Minimum probability of being related to return
DEFAULT_MIN_PREDICTED_LABEL = 0.9

# A threshold of predicted probabily under which
# edit distance is checked (to discard duplicates)
SIMILAR_PREDICTION_TRIGGER_EDIT_DISTANCE_CHECK = 0.0001

# A threshold for edit distance; this is applied twice:
# 1. For pairs of tweets with predictions within SIMILAR_PREDICTION_TRIGGER_EDIT_DISTANCE_CHECK
# 2. For all pairs of the top MAX_TWEETS_CENTRALITY tweets
SIMILAR_PREDICTION_EDIT_DISTANCE_MAX = 0.8

# Maximum tweets to process
MAX_TWEETS_TO_PROCESS = 8000

# Tweets for centrality computation (cost is quadratic on this number, so stay small)
MAX_TWEETS_CENTRALITY = 200

def sample_representative_tweets(tweets, count, min_predicted_label, similar_prediction_trigger_edit_distance_check, similar_prediction_edit_distance_max):

    # Threshold by score and remove duplicate ids if found
    tweets_above_threshold = []
    seen_id = {}
    for tweet in tweets:

        if tweet['label_predicted'] >= min_predicted_label:
            if tweet['id_str'] not in seen_id:
                tweets_above_threshold.append(tweet)
                seen_id[tweet['id_str']] = True
    printe("Above threshold of %.2f: %d tweets" % (min_predicted_label, len(tweets_above_threshold)))
    
    # If there are too many, pick top by score
    if len(tweets_above_threshold) > MAX_TWEETS_TO_PROCESS:
        tweets_above_threshold = sorted( tweets_above_threshold, key=lambda x: x['label_predicted'], reverse=True)
        tweets_above_threshold = tweets_above_threshold[:MAX_TWEETS_TO_PROCESS]
        printe("List truncated to top %d by label, because it exceeded limit" % len(tweets_above_threshold))
    
    # Normalize contents
    for tweet in tweets_above_threshold:
        tweet['_normalized_text'] = tweet_normalization_aggressive(tweet['text'])
    
    # Remove tweets that have similar probability to another and very similar text
    # This exploits the fact that similar tweets will have similar probabilities
    is_duplicate = {}
    multiplicity = {}
    for tu in tweets_above_threshold:
        for tv in tweets_above_threshold:
            if abs(tu['label_predicted'] - tv['label_predicted']) < similar_prediction_trigger_edit_distance_check:
                normalized_edit_similarity = ratio(tu['_normalized_text'], tv['_normalized_text'])
                if normalized_edit_similarity > similar_prediction_edit_distance_max:
                    
                    # The newer tweet (larger id) is marked as a duplicate of the older (smaller id) tweet
                    # Count the == in case there are duplicate ids in the
                    if int(tu['id_str']) < int(tv['id_str']):
                        is_duplicate[tv['id_str']] = tu['id_str']
                        multiplicity[tu['id_str']] = multiplicity[tu['id_str']] + 1 if tu['id_str'] in multiplicity else 1
    
    # Remove duplicates
    tweets_unique = [ tweet for tweet in tweets_above_threshold if tweet['id_str'] not in is_duplicate]
    printe("Unique tweets: %d" % len(tweets_unique))
    
    # Add multiplicity
    for tweet in tweets_unique:
        if tweet['id_str'] in multiplicity:
            tweet['_multiplicity'] = multiplicity[tweet['id_str']]
        else:
            tweet['_multiplicity'] = 1
    
    # Create set for second pass (centrality)
    tweets_for_centrality = tweets_unique[:MAX_TWEETS_CENTRALITY]
    centrality = {}
    for tu in tweets_for_centrality:
        for tv in tweets_for_centrality:
            normalized_edit_similarity = ratio(tu['_normalized_text'], tv['_normalized_text'])
            
            # Compute centrality as sum of similarities
            centrality[tu['id_str']] = centrality[tu['id_str']] + normalized_edit_similarity if tu['id_str'] in centrality else normalized_edit_similarity
            centrality[tv['id_str']] = centrality[tv['id_str']] + normalized_edit_similarity if tv['id_str'] in centrality else normalized_edit_similarity
            
            # Discard duplicates
            if normalized_edit_similarity > similar_prediction_edit_distance_max:
                if int(tu['id_str']) < int(tv['id_str']):
                    is_duplicate[tv['id_str']] = tu['id_str']
            
    # Add centrality and mark centrality=0.0 for duplicates
    for tweet in tweets_for_centrality:
        if tweet['id_str'] in centrality and not tweet['id_str'] in is_duplicate:
            tweet['_centrality'] = centrality[tweet['id_str']]
        else:
            tweet['_centrality'] = 0.0
    
    # Sort by multiplicity and probability of being relevant
    tweets_sorted = sorted( tweets_for_centrality, key=lambda x: float(x['label_predicted'])*float(x['_multiplicity'])*float(x['_centrality']), reverse=True )
        
    # Return top count
    tweets_filtered = tweets_sorted[:count]
    
    return tweets_filtered
    

def do_main():
    # Command-line arguments
    parser = argparse.ArgumentParser(description='Selects representative positive tweets form an input that has been classified')
    parser.add_argument('-i', '--input', type=str, required=True, nargs='*', metavar='FILE_JSON', dest='input_files', help='Input file(s) in JSON format')
    parser.add_argument('-t', '--threshold', type=str, default=DEFAULT_MIN_PREDICTED_LABEL, required=False, metavar='NUMBER', dest='threshold', help='Threshold: will only print tweets above this probability')
    parser.add_argument('-c', '--count', type=int, default=DEFAULT_COUNT_TWEETS, required=False, metavar='COUNT', dest='count', help='Number of tweets to return')
    parser.add_argument('-o', '--output', type=str, required=True, metavar='OUT', dest='out', help='output file')
    args = parser.parse_args()
    
    for input_file in args.input_files:
            printe("Reading from '%s'" % input_file)
            tweets = []
            with io.open(input_file, 'r') as input:
                for line in input:
                    tweet = json.loads(line)

                    # Deal with encapsulated tweets
                    if "tweet" in tweet:
                        label_predicted_tmp = tweet["annotations"]["flood_probability"]["yes"]
                        tweet = tweet["tweet"]
                        tweet["label_predicted"] = label_predicted_tmp

                    if 'label_predicted' in tweet:
                        tweet['label_predicted'] = float(tweet['label_predicted'])
                        tweets.append(tweet)
                    else:
                        die("Tweet '%s' does not have a 'label_predicted' field (need to run the classifier first)" % tweet['id_str'])
    
    # All tweets read
    print("Read %d tweets" % len(tweets))
    
    # Obtain sample and print
    sample = sample_representative_tweets(tweets, int(args.count), float(args.threshold), SIMILAR_PREDICTION_TRIGGER_EDIT_DISTANCE_CHECK, SIMILAR_PREDICTION_EDIT_DISTANCE_MAX)
    print("Sampled tweets: %d" % len(sample))
    
    print("\t".join(['label_predicted', '_multiplicity', '_centrality', '_normalized_text']))
    data=json.dumps(sample)
    with open(args.out, 'w') as outfile:
        outfile.write(data)
    #for tweet in sample:
        #print("%.6f %d %.6f %s %s" % (tweet['label_predicted'], tweet['_multiplicity'], tweet['_centrality'], tweet_url(tweet['id_str'], tweet['user']['screen_name']), printable_truncated(tweet['_normalized_text'], 200)))
     #   print("%.6f %d %.6f %s %s" % (tweet['label_predicted'], tweet['_multiplicity'], tweet['_centrality'], tweet_url(tweet['id_str'], tweet['user']['screen_name']), printable_truncated(tweet['text'], 200)))


if __name__ == "__main__":
    do_main()
    
    
