import re
import langdetect
import sys
import nltk

# Internal codes
NO_LANGUAGE = "no_language"


def printe(message):
    """Prints a message to standard error"""
    sys.stderr.write(str(message) + "\n")


def die(message):
    """Stops execution after printing a message on standard error"""
    printe(message)
    sys.exit(1)


def safe_langdetect(text):
    """Detects language of text, catching possible exceptions"""
    if len(text) == 0:
        return NO_LANGUAGE
    else:
        try:
            return langdetect.detect(text.lower())
        except langdetect.lang_detect_exception.LangDetectException:
            return NO_LANGUAGE


def printable_truncated(text, length):
    text = text.replace("\n", "")
    return (text[:length] + '...') if (len(text) > length) else text

def get_ngrams(text):
    """Obtain words, bigrams, and trigrams from text"""
    tokens = nltk.word_tokenize(text.lower())
    bigrams = list(nltk.ngrams(tokens, 2))  # Convert generator to list
    trigrams = list(nltk.ngrams(tokens, 3)) # Convert generator to list

    return tokens + [" ".join(ngram) for ngram in bigrams + trigrams]


def tweet_normalization_aggressive(text):
    """Perform aggressive normalization of text"""
    # Ampersand
    text = re.sub(r'\s+&amp;?\s+', ' and ', text)
    # User mentions
    text = re.sub(r'@[A-Za-z0-9_]+\b', '_USER_ ', text)
    # Time
    text = re.sub(r"\b\d\d?:\d\d\s*[ap]\.?m\.?\b", '_TIME_', text, flags=re.IGNORECASE)
    text = re.sub(r"\b\d\d?\s*[ap]\.?m\.?\b", '_TIME_', text, flags=re.IGNORECASE)
    text = re.sub(r"\b\d\d?:\d\d:\d\d\b", '_TIME_', text, flags=re.IGNORECASE)
    text = re.sub(r"\b\d\d?:\d\d\b", '_TIME_', text, flags=re.IGNORECASE)
    # URLs
    text = re.sub(r'\bhttps?:\S+', ' _URL_ ', text, flags=re.IGNORECASE)
    # Broken URL at the end of a line
    text = re.sub(r'\s+https?$', ' _URL_', text, flags=re.IGNORECASE)
    # Non-alpha non-punctuation non-digit characters
    text = re.sub(r'[^\w\d\s:\'",.\(\)#@\?!/’_]+', '', text)
    # Newlines and double spaces
    text = re.sub(r'\n', ' ', text)
    text = re.sub(r'\s{2,}', ' ', text)
    # Strip
    text = text.strip()
    return text

def make_normalized_readable(text):
    """Make normalized text readable, for annotation purposes"""
    out = text
    out = re.sub(r'^(_USER_\s+)+', '', out)
    out = re.sub(r'_USER_', '@user', out)
    out = re.sub(r'_URL_', '[url]', out)
    return(out)


def tweetid_from_url(url):
    """Tweet id is determined from the URL because a spreadsheet
    may destroy the 'tweet_id' field"""
    return(re.findall(r'/(\d+)$', url)[0])


def tweet_url(tweet_id, author):
    """Obtains the canonical URL at which Twitter will keep a tweet"""
    return("https://twitter.com/%s/status/%s" % (author, tweet_id))
