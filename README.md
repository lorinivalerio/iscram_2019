# README #

Directory for lab/experimental code.

## Requirements

See `requirements.txt` for required Python3 libraries. You can install all requirements by running:

    pip3 install -r requirements.txt

After installation of the requirements, you need to download the "popular" set of NLTK corpora:

    sudo python3 -m nltk.downloader popular       

Remember to add the base directory for this project (./) to your PYTHONPATH, e.g.:

    PYTHONPATH=$PYTHONPATH:~/workspace/


The training dataset are available at https://zenodo.org/record/2644094

In order to reproduce experiments used for the pape SMFR use the jupyter notebooks in jupyters
The python code in the folder lab (used by jupyters) is described below:

## Classifying

The following must be done to classify tweets:

1. Extract a set of tweets to be annotated to a spreadsheet (.csv file)
2. Annotate them manually in the spreadsheet (locally or with a crowdsourcing service)
3. Consolidate the annotations with the original tweets
4. Train a classifier

### Create a training set

The following extracts a set of tweets to be annotated:

    python3 classify/create_training_set.py --sample-size 2000 --input ~/data/2017_floodquerier/*.stage2.en.json --output ~/inv.sec/2017_jrc_floods/to-annotate/20171120.training.2000.en

If you already have some labeled items, you may want to exclude them:

    python3 classify/create_training_set.py --sample-size 2000 --input ~/data/2017_floodquerier/*.stage2.en.json --exclude ~/inv.sec/2017_jrc_floods/annotated/*.en.json --output ~/inv.sec/2017_jrc_floods/to-annotate/20171120.training.2000.en

It is recommended to use the `--max-per-day` parameter to limit the maximum number
of tweets from a given day and thus diversify the sample.

The file for annotation (.csv) includes the URL of each tweet and its text. The
URL is important not only for the manual annotator to inspect the tweet's context, if
needed, but also because the tweet-id is determined from the URL, given that
some spreadsheet programs may interpret the tweet-id as a floating precision
number and truncate it.

An additional JSON file is created, with the same name, to preserve the entire
tweet in JSON, which is needed later.

### Annotate the training set (locally)

To annotate the training set manually, simply open it with your favorite spreadsheet program and fill-in the `label` column. Every element must have a label, if some elements do not have a label, remove their rows.

### Annotate the training set (using a crowdsourcing service)

You can also use an external service such as [Hybrid](https://www.gethybrid.io/)
for annotating, in this example, we assume you uploaded the spreadsheet, ran an
annotation task, and then downloaded labeled from them. We will assume only
two annotations were requested per element (more can be requested).

First, edit the file `classify/get_hybrid.ini` to enter the exact question
you used (should match the column name of the annotation column in the
file you downloaded), and the possible answers, both in textual form and
as numeric codes.

Second, run the following to average annotations:

    python3 classify/parse_gethybrid_results.py --config classify/get_hybrid.ini --annotated-file ~/inv.sec/2017_jrc_floods/annotated/20171114.training.2000.en.gethybrid.csv --output ~/inv.sec/2017_jrc_floods/annotated/20171114.training.2000-averaged.en.csv

Third, edit the resulting .csv file to resolve ambiguous cases. If you got only two
annotations per element, the ambiguous cases will be non-integer. Ensure every
label is an integer.

### Consolidate the training set

Use the following command to consolidate the .csv file with the annotation with
the original JSON files containing the entire tweets. This is useful in case we
want to use non-textual features from the tweets, and because the annotation file
has transformed the tweets.

    python3  classify/consolidate_training_set.py --input ~/data/2017_floodquerier/*.stage2.en.json --annotated ~/inv.sec/2017_jrc_floods/annotated/20171114.training.2000.en.csv --output ~/inv.sec/2017_jrc_floods/annotated/20171114.training.2000.en.json            

If you have it available, you can also use the JSON file generated while creating
the training set, which contains the same tweets as the .csv file.

### Try different classifiers and discard location words

To train a classifier you need to specify:

1. The input file containing the annotations.
2. The language of the tweets (to remove stopwords)
3. A file with locations and location-specific words (to prevent the classifier from learning those words)

Several algorithms are implemented in `train_classifier.py`. An algorithm that
is known to perform well in text classification tasks is SVM (Support Vector
Machines). To train an SVM, use:

    python3 classify/train_classifier.py --input ~/inv.sec/2017_jrc_floods/annotated/*.training.*.en.json --language en --locations resources/locations.en.txt --test-split 0.2 --algorithm svm

A random sample of tweets of size `--test-split` will be used for evaluating
the resulting classifier. The evaluation is done in terms of precision, recall,
and F1 score, which is the harmonic mean of precision and recall.

In addition to `svm` (which is the default), other algorithms are available:

1. `naive-bayes` (multinomial naive Bayes classifier)
2. `random-forest` (a set of 10 decision trees)
3. `decision-tree` (a decision tree). Use option `--print-tree` to print the tree, which is helpful to understand the classifier.
4. `perceptron` (a simple neural network, slow)

In all cases, a very useful feature is `--print-ngrams 100` (or other number), which
prints the top n-grams used by the classifier. If any of these n-grams is a location
or a location-specific keyword, ad it to the locations.*.txt file. Make sure
to exclude all location or location-specific keywords from the classifier.

### Create a classifier

To create a classifier, set the test proportion to 0.0, so all your examples
are used for training, and specify an output file for the classification model.

    python3 classify/train_classifier.py --input ~/inv.sec/2017_jrc_floods/annotated/*.training.*.en.json --language en --locations resources/locations.en.txt --test-split 0.0 --algorithm svm --output ~/inv.sec/2017_jrc_floods/models/20171114.relevance.en

The output you are given is a base name (e.g., "relevance.en"). Two files will be created,
"relevance.en.model" containing the model, and "relevance.en.vectorizer"
containing the mapping from n-grams to feature numbers in the model.

### Running the classifier

To run the classifier, use:

    python3 classify/run_classifier.py --language en --input ~/data/2017_floodquerier/*.stage2.en.json --model ~/inv.sec/2017_jrc_floods/models/20171114.relevance.en  | less

Each input data will be read completely, passed through the classifier, and then
written to the standard output in decreasing order of probability of belonging
to the positive class.

### (Experimental) Use convolutional neural networks

A Convolutional Neural Network (CNN) classifier can be created.
Three network architectures are included (small, medium, and large).

Learning a CNN takes time. The entire training set is passed through the
network a number of times (called "epochs"). Around 20 epochs is a
safe default (`--epochs 20`).

### Embeddings

Word embeddings are used. They are projections (embeddings) that map
words to numerical vectors of fixed length. The typical number of
dimensions is 100-300.


You can download embedding files for English from the [GloVe project](https://nlp.stanford.edu/projects/glove/).

The number of embedding dimensions must match the dimension of your embedding
files, which are used for initialization. Otherwise the initialization is random.

#### Example: training without initializing embeddings

This is not recommended, as initializing with embeddings yields better results.

    python3 classify/train_cnn_classifier.py --input ~/inv.sec/2017_jrc_floods/annotated/*.training.*.en.json --locations resources/locations.en.txt --embeddings-dimension 100 --network-size small --output ~/inv.sec/2017_jrc_floods/models/20171122.relevance-cnn.en --epochs 20

To run it:

    python3 classify/run_cnn_classifier.py --input /home/chato/data/2017_floodquerier/*.stage2.en.json --model ~/inv.sec/2017_jrc_floods/models/20171122.relevance-cnn.en

#### Example: training with initialization

This has the same number of parameters as above (the number of parameters
depends on the embedding dimension and the number of layers).

    python3 classify/train_cnn_classifier.py --input ~/inv.sec/2017_jrc_floods/annotated/*.training.*.en.json --locations resources/locations.en.txt --embeddings-file ~/data/2017_word_embeddings/glove.twitter.27B.100d.txt --embeddings-dimension 100 --network-size small --output ~/inv.sec/2017_jrc_floods/models/20171122.relevance-cnn-init.en --epochs 20

To run it:

    python3 classify/run_cnn_classifier.py --input /home/chato/data/2017_floodquerier/*.stage2.en.json --model ~/inv.sec/2017_jrc_floods/models/20171122.relevance-cnn-init.en

#### Example: training with initialization, Spanish

Training on Spanish annotations; note the initialization embeddings must be for Spanish also.

    python3 classify/train_cnn_classifier.py --input  ~/inv.sec/2018_jrc_floods/annotated/*.training.*.es.json --locations resources/locations.es.txt --embeddings-dimension 300 --embeddings-file ~/data/2017_word_embeddings/wordvectors.300d.es.vec --network-size small --epochs 10 --batch-size 150 --output ~/inv.sec/2018_jrc_floods/models/20180320.relevance-cnn-init.es

### Running a CNN classifier

The CNN classifier can be ran using the following command:

    python3 classify/run_cnn_classifier.py --input ~/data/2017_floodquerier/*.filter.stage2.en.json --model ~/inv.sec/2018_jrc_floods/models/20180319.relevance-cnn-init.en

This will load one file at a time, run the classifier over the entire file,
and then output a sorted list of messages by descending probability of being flood-related.

This is useful to do the following: look for (1) errors, and (2) borderline cases having neither a 1.000 nor a 0.000 score; these cases can be forced into a new training set by using the --include parameter of the create training set program, to focus the collection of labels into those cases.
