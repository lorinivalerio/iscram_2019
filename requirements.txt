# Twitter collector
twython>=3.6.0

# Text libraries
geotext>=0.3.0
langdetect>=1.0.7
nltk>=3.1
stop-words>=2015.2.23.1

# Classification libraries
Keras>=2.1.1
sklearn>=0.0
Theano>=0.9.0

# Data library for Keras
h5py>=2.7.1

# Edit distance
Levenshtein
